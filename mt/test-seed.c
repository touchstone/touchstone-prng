#include <stdio.h>
#include <stdlib.h>

void init_genrand64(unsigned long long);
double genrand64_real1(void);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 100000000;

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		init_genrand64(1234567890 + i);
	}
	return 0;
}
