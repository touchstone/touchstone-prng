#include <stdio.h>
#include <stdlib.h>

void init_genrand64(unsigned long long);
double genrand64_real1(void);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;

	init_genrand64(1234567890);

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		genrand64_real1();
	}
	return 0;
}
