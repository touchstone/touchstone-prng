all:
	cd mt && $(MAKE)
	cd pcg && $(MAKE); $(MAKE) -f Makefile.touchstone
	cd xorshift && $(MAKE)
	cd xorshiftplus && $(MAKE)
	cd xorshiftstar && $(MAKE)
	cd xoshiro256plusplus && $(MAKE)
	cd xoshiro256starstar && $(MAKE)

clean:
	cd mt && $(MAKE) clean
	cd pcg && $(MAKE) clean; $(MAKE) -f Makefile.touchstone clean
	cd xorshift && $(MAKE) clean
	cd xorshiftplus && $(MAKE) clean
	cd xorshiftstar && $(MAKE) clean
	cd xoshiro256plusplus && $(MAKE) clean
	cd xoshiro256starstar && $(MAKE) clean
