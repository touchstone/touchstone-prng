#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void seed(uint64_t);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;


	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		seed(1234567890 + i);
	}
	return 0;
}
