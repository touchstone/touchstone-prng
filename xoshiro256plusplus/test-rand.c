#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void seed(uint64_t);
uint64_t next(void);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 100000000;

	seed(1234567890);

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		next();
	}
	return 0;
}
