#include <stdio.h>
#include <stdlib.h>
#include "pcg_variants.h"

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;

	pcg64s_random_t rng;

	pcg64s_srandom_r(&rng, 1234567890);

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		pcg64s_random_r(&rng);
	}
	return 0;
}
