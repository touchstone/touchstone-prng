#include <stdio.h>
#include <stdlib.h>
#include "pcg_variants.h"

int main(int argc, char *argv[])
{
	unsigned long long i, r1, r2;
	unsigned long long max = 10000000000;

	pcg64_random_t rng1;
	pcg64_random_t rng2;

	pcg64s_random_t rngs1;
	pcg64s_random_t rngs2;

	pcg64f_random_t rngf1;
	pcg64f_random_t rngf2;

	printf("pcg64:\n");
	pcg64_srandom_r(&rng1, 0, 1234567890);
	for (i = 0; i < 5; i++) {
		r1 = pcg64_random_r(&rng1);

		pcg64_srandom_r(&rng2, 0, 1234567890);
		pcg64_advance_r(&rng2, i);
		r2 = pcg64_random_r(&rng2);

		printf("  %llu: %llu == %llu is %d\n", i, r1, r2, r1 == r2);
	}

	printf("pcg64s:\n");
	pcg64s_srandom_r(&rngs1, 1234567890);
	for (i = 0; i < 5; i++) {
		r1 = pcg64s_random_r(&rngs1);

		pcg64s_srandom_r(&rngs2, 1234567890);
		pcg64s_advance_r(&rngs2, i);
		r2 = pcg64s_random_r(&rngs2);

		printf("  %llu: %llu == %llu is %d\n", i, r1, r2, r1 == r2);
	}

	printf("pcg64f:\n");
	pcg64f_srandom_r(&rngf1, 1234567890);
	for (i = 0; i < 5; i++) {
		r1 = pcg64f_random_r(&rngf1);

		pcg64f_srandom_r(&rngf2, 1234567890);
		pcg64f_advance_r(&rngf2, i);
		r2 = pcg64f_random_r(&rngf2);

		printf("  %llu: %llu == %llu is %d\n", i, r1, r2, r1 == r2);
	}

	return 0;
}
