#include <stdio.h>
#include <stdlib.h>
#include "pcg_variants.h"

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;

	pcg64_random_t rng;

	pcg64_srandom_r(&rng, 0, 1234567890);

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		pcg64_random_r(&rng);
	}
	return 0;
}
