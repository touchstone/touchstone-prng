#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "pcg_variants.h"

#define RANGE 100

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;
	int j;
	int a[RANGE];

	pcg64f_random_t rng;

	pcg64f_srandom_r(&rng, 1234567890);

	if (argc == 2)
		max = atoll(argv[1]);

	memset(a, 0, sizeof(int) * RANGE);
	for (i = 0; i < max; i++) {
		int r = (int) (RANGE * ldexp(pcg64f_random_r(&rng), -64));
		++a[r];
	}
	for (j = 0; j < RANGE; j++) {
		printf(" %03d: %0.3f", j, (double) a[j] / (double) max);
		if ((j +1) % 5 == 0)
			printf("\n");
		else
			printf("  ");
	}
	return 0;
}
