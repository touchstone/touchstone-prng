#include <stdio.h>
#include <stdlib.h>
#include "pcg_variants.h"

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 100000000;

	pcg64_random_t rng;

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		pcg64f_srandom_r(&rng, 1234567890 + i);
	}
	return 0;
}
