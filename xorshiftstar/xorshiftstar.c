/* Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. */

#include <stdint.h>

struct xorshift64s_state {
  uint64_t a;
};

uint64_t xorshift64s(struct xorshift64s_state *state)
{
	uint64_t x = state->a;	/* The state must be seeded with a nonzero value. */
	x ^= x >> 12; // a
	x ^= x << 25; // b
	x ^= x >> 27; // c
	state->a = x;
	return x * UINT64_C(0x2545F4914F6CDD1D);
}
