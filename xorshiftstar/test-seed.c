#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct xorshift64s_state {
	uint64_t a;
};

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 10000000000;

	struct xorshift64s_state seed;
	seed.a = 1234567890;

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		seed.a += 1;
	}
	return 0;
}
