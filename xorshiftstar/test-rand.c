#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct xorshift64s_state {
	uint64_t a;
};

uint64_t xorshift64s(struct xorshift64s_state *);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 100000000;

	struct xorshift64s_state seed;
	seed.a = 1234567890;

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		xorshift64s(&seed);
	}
	return 0;
}
