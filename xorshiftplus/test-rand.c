#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct xorshift128p_state {
	uint64_t a, b;
};

uint64_t xorshift128p(struct xorshift128p_state *);

int main(int argc, char *argv[])
{
	unsigned long long i;
	unsigned long long max = 100000000;

	struct xorshift128p_state seed;
	seed.a = 12345;
	seed.b = 67890;

	if (argc == 2)
		max = atoll(argv[1]);

	for (i = 0; i < max; i++) {
		xorshift128p(&seed);
	}
	return 0;
}
